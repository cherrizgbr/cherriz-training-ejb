package de.cherriz.training.ejb.business;

import de.cherriz.training.ejb.simplecalc.SimpleCalculator;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

@Stateless(mappedName = "BusinessService")
@Remote({BusinessService.class})
public class BusinessServiceImpl implements BusinessService {

    @EJB
    private SimpleCalculator calculator;

    @Override
    public Long ergaenzeVersandkosten(Long preis) {
        Long versandkosten = 10L;
        return calculator.plus(preis, versandkosten);
    }
}