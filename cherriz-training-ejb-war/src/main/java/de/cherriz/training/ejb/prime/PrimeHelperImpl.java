package de.cherriz.training.ejb.prime;

import javax.ejb.Remote;
import javax.ejb.Stateless;

@Stateless(mappedName = "PrimeHelper")
@Remote({PrimeHelper.class})
public class PrimeHelperImpl implements PrimeHelper {

    @Override
    public Boolean isPrime(Long number) {
        for (long i = 2; i < number / 2; i++) {
            if (number % i == 0) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

}