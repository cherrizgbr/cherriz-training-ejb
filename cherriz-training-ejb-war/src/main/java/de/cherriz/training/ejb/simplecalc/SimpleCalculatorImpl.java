package de.cherriz.training.ejb.simplecalc;

import javax.ejb.Remote;
import javax.ejb.Stateless;

@Stateless(mappedName = "SimpleCalculator")
@Remote({SimpleCalculator.class})
public class SimpleCalculatorImpl implements SimpleCalculator {

    @Override
    public Long plus(Long a, Long b) {
        return a + b;
    }

    @Override
    public Long minus(Long a, Long b) {
        return a - b;
    }

}