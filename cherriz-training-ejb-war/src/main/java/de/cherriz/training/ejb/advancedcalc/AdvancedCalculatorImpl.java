package de.cherriz.training.ejb.advancedcalc;

import javax.ejb.Remote;
import javax.ejb.Stateless;

@Stateless(mappedName = "AdvancedCalculator")
@Remote({AdvancedCalculator.class})
public class AdvancedCalculatorImpl implements AdvancedCalculator {

    @Override
    public Long plus(Long a, Long b) {
        return a + b;
    }

    @Override
    public Long minus(Long a, Long b) {
        return a - b;
    }

    @Override
    public Long multiply(Long a, Long b) {
        return a * b;
    }

    @Override
    public Long divide(Long a, Long b) {
        return a / b;
    }

}