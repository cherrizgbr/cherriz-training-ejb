package de.cherriz.training.ejb.simplecalc;

import java.io.Serializable;

public interface SimpleCalculator extends Serializable {

    Long plus(Long a, Long b);

    Long minus(Long a, Long b);

}