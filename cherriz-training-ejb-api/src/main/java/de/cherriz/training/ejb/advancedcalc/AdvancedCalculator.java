package de.cherriz.training.ejb.advancedcalc;

import java.io.Serializable;

public interface AdvancedCalculator extends Serializable  {

    Long plus(Long a, Long b);

    Long minus(Long a, Long b);

    Long multiply(Long a, Long b);

    Long divide(Long a, Long b);

}