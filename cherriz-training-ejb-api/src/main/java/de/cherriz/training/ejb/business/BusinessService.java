package de.cherriz.training.ejb.business;

import java.io.Serializable;

public interface BusinessService extends Serializable {

    Long ergaenzeVersandkosten(Long preis);

}