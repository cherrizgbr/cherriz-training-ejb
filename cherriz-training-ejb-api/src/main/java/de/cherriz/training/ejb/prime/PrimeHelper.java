package de.cherriz.training.ejb.prime;

import java.io.Serializable;

public interface PrimeHelper extends Serializable {

    Boolean isPrime(Long number);

}