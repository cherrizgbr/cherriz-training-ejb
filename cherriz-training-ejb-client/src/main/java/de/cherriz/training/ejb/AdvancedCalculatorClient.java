package de.cherriz.training.ejb;

import de.cherriz.training.ejb.advancedcalc.AdvancedCalculator;

public class AdvancedCalculatorClient {

    public static void main(String[] args) {
        AdvancedCalculator calc = EJBFactory.lookUp(AdvancedCalculator.class);
        System.out.println(args[0] + " * " + args[1] + " = " + calc.multiply(Long.parseLong(args[0]),
                                                                             Long.parseLong(args[1])));
    }

}
