package de.cherriz.training.ejb;

import de.cherriz.training.ejb.business.BusinessService;

public class BusinessServiceClient {

    public static void main(String[] args) {
        BusinessService service = EJBFactory.lookUp(BusinessService.class);
        System.out.println(args[0] + " + Versandkosten = " + service.ergaenzeVersandkosten((Long.parseLong(args[0]))));
    }

}
