package de.cherriz.training.ejb;

import de.cherriz.training.ejb.prime.PrimeHelper;

public class PrimeHelperClient {

    public static void main(String[] args) {
        PrimeHelper helper = EJBFactory.lookUp(PrimeHelper.class);

        if(helper.isPrime(Long.valueOf(args[0]))){
            System.out.println(args[0] + " ist EINE Primzahl.");
        }else {
            System.out.println(args[0] + " ist KEINE Primzahl.");
        }
    }

}
