package de.cherriz.training.ejb;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class EJBFactory {

    private static InitialContext ctx = null;

    private static Map<Class, Serializable> instances = new HashMap<>();

    private static void initCtx() throws NamingException {
        // Für lokalen Zugriff: http://127.0.0.1:8080/tomee/ejb
        initCtx("org.apache.openejb.client.RemoteInitialContextFactory", "http://mib-hp-sose19.iap.hs-heilbronn.de:8080/tomee/ejb");
    }

    private static void initCtx(String factory, String url) throws NamingException {
        Properties p = new Properties();
        p.put("java.naming.factory.initial", factory);
        p.put("java.naming.provider.url", url);
        ctx = new InitialContext(p);
    }

    public static <T extends Serializable> T lookUp(Class<T> clazz) {
        if (instances.get(clazz) == null) {
            try {
                if (ctx == null) {
                    initCtx();
                }
                T instance = (T) ctx.lookup(clazz.getSimpleName());
                instances.put(clazz, instance);
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }
        return (T) instances.get(clazz);
    }

}
