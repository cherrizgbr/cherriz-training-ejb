package de.cherriz.training.ejb;

import de.cherriz.training.ejb.simplecalc.SimpleCalculator;

public class SimpleCalculatorClient {

    public static void main(String[] args) {
        SimpleCalculator calc = EJBFactory.lookUp(SimpleCalculator.class);
        System.out.println(args[0] + " + " + args[1] + " = " + calc.plus(Long.parseLong(args[0]),
                                                                         Long.parseLong(args[1])));
    }

}
